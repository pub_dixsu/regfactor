# описание

регистрация аторизация через временный фактор, отправки временного пароля на почту
консольное приложение, в котором созданы все интерфейсы для работы функционала.

организована логика работы со списком пользователей ```users``` объявленных в ```defs/maildefs.py```, 
которую можно впоследствии прикрутить и использовать с любой базой данных

процесс авторизации происходит по временному паролю, который высылается пользователю на
email с которым он зарегестрирован на сервисе. авторизация сохраняется в сессии устройства пользователя
а пароль удаляется. чтобы зайти в аккаунт с другого устройства, необъодимо будет заново получить временный
пароль на свой email, в момент авторизации

в последствии у данного модуля должен быть прямой доступ к базе данных приложения к модели user
чтобы иметь возможность менять данные в базе. а также сделать это через web интерефейс например

---

# необходимые действия

## для запуска приложения через ```Docker``` просто в корне с ```Dockerfile``` создайте ```.env``` и  запустите ```./start.sh```

* .env
```
SMTP_SERVER=xxx.xxx.xx
SMTP_USER=xxxx@xxx.xx
SMTP_PASSWORD=xxxxxxxxxxx
SMTP_PORT=587
EMAIL_USE_TLS=True
```
необходимо указывать свои данные, для некоторых почтовых серверов таких как ```mail.ru``` потребуется указывать 
пароль для внешнего приложения


в пакете ```defs/usersdefs``` в словарь users можете сразу указать пользователя со своим настоящим ```email``` для проверки функционала
или создайте его уже в запущенном приложении, через функционал приложения

в ```defs/userdefs``` в функции ```_reset_after_time``` можно указать любое время задержки перед обнулением пароля сейчас
стоит 5*4 это 20 секунд...

---
## запуск локально ```без Docker```


в каталог ```defs/``` положить файл ```.env``` с тем же содержимым что и для Docker
для возможности отправки email с указанного почтового ящика


* для правильной (без зависаний) работы библиотеки ```smptlib``` лучше работать без vpn

```
git clone https://gitlab.com/pub_dixsu/regfactor.git 
cd regfactor
```

создание файла с данным для почтового сервера
```
vi defs/.env
```

* создание окружения и активация окружения


```
python3 -m venv venv
pip install -r requirements.txt
source venv/bin/activate
```

* запуск приложения в созданном окружении


```
python my.py
```

* завершить работу в окружении

```
deactivate
```




---
# Лицензия
* модуль публикуется под CC BY-SA 4.0.
> My module regfactor
> Copyright (C) 2023  Roman Sakhno (ramanzes)
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as published
> by the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.
 
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
* файл полного текста лицензии legalcode.txt находится в каталоге с проектом





