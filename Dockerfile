FROM python:3.12



RUN apt update
RUN apt upgrade -y
RUN apt install git -y 

#RUN echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
#RUN echo $(ls /etc/ssh/)

#CMD ["/bin/bash"]

WORKDIR /home
RUN git clone https://gitlab.com/pub_dixsu/regfactor.git

WORKDIR /home/regfactor

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

#в каталоге с Dockerfile создайте файл .env с данными mail сервера
COPY .env ./defs/

#CMD ["/bin/bash"]
CMD [ "python", "my.py" ] 


#запуск приложения

