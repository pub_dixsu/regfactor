import threading
import time

def reset_after_5_minutes(variable):
    # Ждем 5 секунд
    time.sleep(1 * 5)
    # Обнуляем значение переменной
    variable.value = 0
    print(variable.value)

# Создаем переменную, которую мы хотим обнулить
variable = threading.local()
variable.value = 10 # Присваиваем переменной значение

# Создаем и запускаем поток, который будет обнулять переменную через 5 минут
reset_thread = threading.Thread(target=reset_after_5_minutes, args=(variable,))
reset_thread.start()

# Продолжаем выполнение основного потока
print(variable.value)
print("Переменная будет обнулена через 5 секунд")

print(variable.value)
