
import random
import string

from defs.exceptions import SetCodeError

def generate_random_code(length=8):
    """здесь создаётся ранодмный код в переменной code_now"""
    # Генерация случайного кода из букв и цифр
    try:
        characters = string.ascii_letters + string.digits
        random_code = ''.join(random.choice(characters) for _ in range(length))
        return random_code
    except:
        code_now = False
        raise SetCodeError
        exit(1)


if __name__ == "__main__":
   # создать свой модуль исключений
    code_now = generate_random_code()
    print(code_now)


