import smtplib
import sys
from email.message import EmailMessage
import datetime
#нужно в окружение добавить pytz
import pytz

from dotenv import load_dotenv
import os
from defs.myclasses import Users,Email,User,Sender
from defs.exceptions import MailSendError

load_dotenv()


#инициализация сервера отправителя
def _mailserver() -> Sender:
    """здесь я получаю авторизационные данные в переменной mailserver для отправки почты с этого ящика"""
    return _getSender()

def _getSender() -> Sender:
    try:
        dir(os.getenv)
        send=Sender(os.getenv("SMTP_SERVER"), os.getenv("SMTP_USER"), os.getenv("SMTP_PASSWORD"), os.getenv("SMTP_PORT"), os.getenv("EMAIL_USE_TLS"))
    except Exception as Err:
        print(Err)
    return send


def sendmailto(user:Users,code:str) -> bool:
    """ функция отправки сообщения пользователю"""

    sender:Sender
    #инициализация сервера отправителя
    sender = _mailserver()
    # Create message
    msg = EmailMessage()
    msg["Subject"] = "Ваш код" 
    msg["From"] = sender.SMTP_USER
    msg["To"] = str(user.user)

    # Create datetime object in UTC
    utc_datetime = datetime.datetime.now(datetime.UTC).replace(tzinfo=pytz.UTC)


    # Format it as a string
    date_string = utc_datetime.strftime('%a, %d %b %Y %H:%M:%S %z')

    # Set Date header
    msg['Date'] = date_string

    msg.set_content(code)
    
    #try:
    with smtplib.SMTP(sender.SMTP_SERVER, sender.SMTP_PORT) as server:
        server.starttls()
        server.login(sender.SMTP_USER, sender.SMTP_PASSWORD)
        server.send_message(msg)
        server.quit()
        return True

    #except Exception as Err:
     #   raise Err
     #   raise MailSendError("Ошибка при отправке почты") from Err
    
    return False



