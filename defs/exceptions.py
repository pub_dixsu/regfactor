

class Error(Exception):
    """базовый класс всех исключений"""
    # def __init__(self, message, extra_info):
    #     super().__init__(message)
    #     self.extra_info = extra_info
    #
    pass

class SetCodeError(Error):
    """Не можем сгенерировать код"""
    pass


class MailSendError(Error):
    """не можем отправить сообщение"""
    pass
