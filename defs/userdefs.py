from typing import Iterable
from dataclasses import dataclass

from defs.exceptions import MailSendError

from defs.myclasses import Users,Email,User 
# Создаем словарь, где ключами будут адреса электронной почты пользователей,
# а значениями будут сами объекты Users
users = {
    'aa@aa.aa': Users(user='aa@aa.aa', psw=None),
    'aa1@aa.aa': Users(user='aa1@aa.aa', psw=None),
    'aa2@aa.aa': Users(user='aa2@aa.aa', psw=None),
    'aa3@aa.aa': Users(user='aa3@aa.aa', psw=None),
}





def finduser(mail:Email) -> Users:
    return _finduser(mail)


def _finduser(mail:Email) -> Users:
    user:Users
    if mail in users:
        print(f'пользователь {mail} найден')
        user=users[mail]
    else:
        print(f"пользователь {mail} не найден")
        user=None
    return user   


def createuser(mail:Email) -> Users:
    print('Пользователя с таким email нет в базе данных')
    var = input(f"Введите цифру 1, чтобы создать нового пользователя с {mail}, или введите другой email чтобы найти свой существующий аккаунт:")
    if var == '1':
        user=_create_new_user(mail)
    else:
        user=_finduser(var)
    return user


def _create_new_user(mail:Email) -> Users:
    # Добавляем нового пользователя
    new_user = Users(user=mail, psw=None)
    users[mail] = new_user
    return new_user



#авторизован ли пользователь, и если нет, то авторизовать
def authuser(user:Users):
    if not _auth_in_ses(user):
        _setcode(user)
        return False
    return True

def loginuser(user:Users,code:str):
    if user.psw is None or user.psw != code:
        return False
    else:
        _setuserauth(user)
        return True


#проверяет авторизован ли пользователь в сесии
def _auth_in_ses(user:Users):
    #заглушка всегда False
    return user.isauth

#авторизация пользователя в сессии
def _setuserauth(user:Users):
    user.isauth=True

def printAlluser():
    print("Список всех пользователей:")
    for usr in users:
        print(f"Пользователь {users[usr].user} пароль {users[usr].psw}")


from defs.create_code import generate_random_code
from defs.maildefs import sendmailto

import threading
import time

#генерация и установка пароля пользователю
def _setcode(user:Users) -> bool:
    code=generate_random_code()
    
    user.psw=code
    
    #в этой функции может вызываться исключение
    try:
        sendmailto(user,code)
    except Exception as Err:
        #таким образом мы ловим все исключения возникающие при sendmailto, и заворачиваем их в своё исключение и передаём в Err в обработчик на уровень выше
        raise MailSendError(Err)

    
    # Создаем и запускаем поток, который будет обнулять переменную через 5 минут
    reset_thread = threading.Thread(target=_reset_after_time, args=(user,))
    reset_thread.start()
    print(f'пароль выслан на почту {user.user}')
    #print(f'1)пароль для пользователя {user.user} установлен в {user.psw}')
    return True


def _reset_after_time(user:Users):
    #Ждем 20 сек
    time.sleep(4 * 5)
    #Обнуляем значение переменной
    user.psw = None 
    print(f'2)пароль пользователя {user.user} установлен в {user.psw}')
   # printAlluser()


