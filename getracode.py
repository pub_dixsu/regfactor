#!!!!!!!!!!!!!!!!!!getracode!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
import random
import string

def generate_random_code(length=8):
    # Генерация случайного кода из букв и цифр
    characters = string.ascii_letters + string.digits
    random_code = ''.join(random.choice(characters) for _ in range(length))
    return random_code


try:
    code = generate_random_code()
    #print(random_code)
#   return random_code
except Exception as Err:
    code = False
    print(Err)


if __name__ == "__main__":
    print(code)


# Пример использования
#random_code = generate_random_code()
#print(random_code)
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

